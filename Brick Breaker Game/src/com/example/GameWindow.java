package com.example;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.Timer;

public class GameWindow extends JPanel {
	
	public int height;
	public int width;
	
	public int frameRate;
	public int ballSpeed;
	
	public int highscore;
	
	public boolean gameScreen = false;
	
	GameLogic gameLogic;
	
	JRadioButton collisionCheatOn;
	JRadioButton collisionCheatOff;
	JRadioButton infiniteLivesOn;
	JRadioButton infiniteLivesOff;
	Button play;
	Button quit;
	
	Graphics g;
	
	Timer eventsTimer;
	public boolean paused = false;
	

	public GameWindow(int width, int height, int frameRate, int ballSpeed) {
		this.width = width;
		this.height = height;
		this.frameRate = frameRate;
		this.ballSpeed = ballSpeed;
		
		this.setBackground(Color.BLACK);
		this.gameLogic = new GameLogic(width, height, this);
		MouseAdapter mouseAdapter = new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				gameLogic.handleMouseMoveEvent(e);
			}
			public void mouseClicked(MouseEvent e) {
				gameLogic.handleMouseClickEvent(e);
			}
		};
		this.addMouseListener(mouseAdapter);
		this.addMouseMotionListener(mouseAdapter);
		
		this.setFocusable(true);
		this.requestFocus();
		this.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
					gameLogic.endGame();
				}
				if (ke.getKeyCode() == KeyEvent.VK_P) {
					if (!paused) {
						stopEvents();
					}
					else {
						runEvents();
					}
					paused = !paused;
				}
			}
		});
		
		this.setLayout(null);
		
	    play = new Button("Play");
	    play.setFont(new Font("TimesRoman", Font.PLAIN, 20));
	    play.setBounds(width/2 - 110, 400, 70, 50);
	    play.setBackground(Color.WHITE);
	    play.setForeground(Color.BLACK);
	    play.addMouseListener(new MouseAdapter() {
	    	public void mouseClicked(MouseEvent e) {
	    		setSplashButtons(false);
	    		gameScreen = true;
	    		if (paused) {
	    			runEvents();
	    			paused = false;
	    		}
	    	}
	    });
	    this.add(play);
	    
	    quit = new Button("Quit");
	    quit.setFont(new Font("TimesRoman", Font.PLAIN, 20));
	    quit.setBounds(width/2 + 40, 400, 70, 50);
	    quit.setBackground(Color.WHITE);
	    quit.setForeground(Color.BLACK);
	    quit.addMouseListener(new MouseAdapter() {
	    	public void mouseClicked(MouseEvent e) {
	    		System.exit(0);
	    	}
	    });
	    this.add(quit);
	    
	    this.setVisible(true);
	}
	
	public void paintComponent(Graphics g) {
		this.g = g;
		super.paintComponent(g); // clear the previous paint
		if (gameScreen) {
			gameLogic.paint(g);
		}
		else {
			paintSplashScreen(g);
		}
	}
	
	public void paintSplashScreen(Graphics g) {
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 50));
		drawCenterString(g, "The Brick Bricker", 100, 0);
		
		drawCenterString(g, "Beat the High Score!", 250, 0);
	}
	
	public void setSplashButtons(boolean on) {
		this.play.setVisible(on);
		this.quit.setVisible(on);
	}
	
	public void drawCenterString(Graphics g, String myString, int y, int xOffset) {
		FontMetrics fm = g.getFontMetrics();
		Rectangle2D rect2D = fm.getStringBounds(myString, g);
		int x = (width - (int)(rect2D.getWidth())) / 2;
		x += xOffset;
		g.drawString(myString, x, y);
	}
	
	public void repaintWithFrameRate(int frameRate) {
		int delay = (int)(1000 / frameRate); //milliseconds
		ActionListener taskPerformer = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				repaint();
			}
		};
		new Timer(delay, taskPerformer).start();
	}
	
	public void runEvents() {
		int delay = this.ballSpeed; //milliseconds
		ActionListener taskPerformer = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				gameLogic.runGame();
			}
		};
		eventsTimer = new Timer(delay, taskPerformer);
		eventsTimer.start();
	}
	
	public void stopEvents() {
		eventsTimer.stop();
	}
	
	public void resizeWindow() {
		Rectangle bounds = this.getBounds();
		this.width = (int)bounds.getWidth();
		this.height = (int)bounds.getHeight();
		play.setLocation(width/2 - 110, 400);
		quit.setLocation(width/2 + 40, 400);
		
		gameLogic.resizeGame(width, height);
		
		repaint();
	}
}
